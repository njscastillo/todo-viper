//
//  LoginLoginViewTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class LoginViewTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    class MockLoginViewOutput: LoginViewOutput {
        var isTouchedLogin =  false
        
        func touchLogin() {
            isTouchedLogin = true
        }
    }
    
    func testGetCredentials() {
        let loginViewController = LoginViewController()
        
        let usernameTextField = UITextField()
        let passwordTextField = UITextField()
        
        usernameTextField.text = "Foo"
        passwordTextField.text = "Bar"
        
        loginViewController.usernameTextField = usernameTextField
        loginViewController.passwordTextField = passwordTextField
        
        loginViewController.getCredentials { (username, password) in
            XCTAssertEqual(username, "Foo")
            XCTAssertEqual(password, "Bar")
        }
    }
    
    func testtouchLoginButton() {
        let loginViewController = LoginViewController()
        let output = MockLoginViewOutput()
        loginViewController.output = output
        loginViewController.touchLoginButton((Any).self)
        XCTAssertTrue(output.isTouchedLogin)
    }
}
