//
//  LoginLoginPresenterTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class LoginPresenterTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    class MockInteractor: LoginInteractorInput {
        var isAuthenticationed = false
        func authentication(credintals: Credentials, completion: (Bool) -> Void) {
            if
                credintals.username == "Foo" &&
                credintals.password == "Bar" {
                isAuthenticationed = true
                completion(true)
            } else {
                isAuthenticationed = false
                completion(false)
            }
        }
    }

    class MockRouter: LoginRouterInput {
        public var isShowTodoList = false
        func showTodoList() {
            isShowTodoList = true
        }
    }

    class MockViewController: LoginViewInput {
        var countGetCredentials = true
        func getCredentials(completion: (String, String) -> Void) {
            if countGetCredentials {
                completion("Foo","Bar")
            } else {
                completion("Foo","Baz")
            }
            countGetCredentials.toggle()
        }
        
        func setupInitialState() {

        }
    }
    
    func testTouchLogin() {
        
        let loginPresenter = LoginPresenter()
        let view = MockViewController()
        let interactor = MockInteractor()
        let router = MockRouter()
        
        loginPresenter.interactor = interactor
        loginPresenter.router = router
        loginPresenter.view = view
        
        loginPresenter.touchLogin()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            XCTAssertEqual(router.isShowTodoList, true)
        }
        
        loginPresenter.touchLogin()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            XCTAssertEqual(router.isShowTodoList, false)
        }
    }
}
