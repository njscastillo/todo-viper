//
//  TaskDetailTaskDetailPresenterTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class TaskDetailPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    class MockInteractor: TaskDetailInteractorInput {
        func setTaskName(task: Task, newValue: String, completion: (Bool) -> Void) {
            
        }
        
        func setTaskDescription(task: Task, newValue: String, completion: (Bool) -> Void) {
            
        }
        
        func setTaskImage(task: Task, newValue: UIImage, completion: (Bool) -> Void) {
            
        }
        
        func removeTaskImage(task: Task, completion: (Bool) -> Void) {
            
        }
        
    }
    
    class MockRouter: TaskDetailRouterInput {
        func showImageMenu(task: Task) {
            
        }
        
        func showImagePicker() {
            
        }
        
        func imagePickerDismiss() {
            
        }
        

    }
    
    class MockViewController: TaskDetailViewInput {
        func setup(task: Task) {
            
        }
        
        func updateTaskImage(image: UIImage?) {
            
        }

        func setupInitialState() {

        }
    }
}
