//
//  ImageMenuImageMenuConfiguratorTests.swift
//  todo
//
//  Created by // on 25/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class ImageMenuModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ImageMenuModuleConfigurator().configure(with: Task(name: "Foo"))

        //when
        //viewController = configurator.configure(with: Task(name: "Foo"))

        //then
        XCTAssertNotNil(viewController.output, "ImageMenuViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ImageMenuPresenter, "output is not ImageMenuPresenter")

        let presenter: ImageMenuPresenter = viewController.output as! ImageMenuPresenter
        XCTAssertNotNil(presenter.view, "view in ImageMenuPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ImageMenuPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ImageMenuRouter, "router is not ImageMenuRouter")

        let interactor: ImageMenuInteractor = presenter.interactor as! ImageMenuInteractor
        XCTAssertNotNil(interactor.output, "output in ImageMenuInteractor is nil after configuration")
    }

//    class ImageMenuViewControllerMock: ImageMenuViewController {
//
//        var setupInitialStateDidCall = false
//
//        override func setupInitialState() {
//            setupInitialStateDidCall = true
//        }
//    }
}
