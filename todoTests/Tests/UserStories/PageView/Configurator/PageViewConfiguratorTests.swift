//
//  PageViewPageViewConfiguratorTests.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest

class PageViewModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = PageViewViewControllerMock()
        let configurator = PageViewModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "PageViewViewController is nil after configuration")
        XCTAssertTrue(viewController.output is PageViewPresenter, "output is not PageViewPresenter")

        let presenter: PageViewPresenter = viewController.output as! PageViewPresenter
        XCTAssertNotNil(presenter.view, "view in PageViewPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in PageViewPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is PageViewRouter, "router is not PageViewRouter")

        let interactor: PageViewInteractor = presenter.interactor as! PageViewInteractor
        XCTAssertNotNil(interactor.output, "output in PageViewInteractor is nil after configuration")
    }

    class PageViewViewControllerMock: PageViewViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
