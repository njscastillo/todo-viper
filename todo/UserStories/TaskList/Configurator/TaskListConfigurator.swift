//
//  TaskListTaskListConfigurator.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

///Viper module configurator

class TaskListModuleConfigurator {

    /// - parameter viewInput: UIViewController
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? TaskListViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: TaskListViewController) {

        let router = TaskListRouter()
        router.view = viewController
        
        let presenter = TaskListPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TaskListInteractor()
        interactor.output = presenter
        interactor.taskListService = TaskListServiceRealm.shared

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
