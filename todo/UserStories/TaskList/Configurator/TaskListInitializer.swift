//
//  TaskListTaskListInitializer.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TaskListModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var tasklistViewController: TaskListViewController!

    override func awakeFromNib() {

        let configurator = TaskListModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: tasklistViewController)
    }

}
