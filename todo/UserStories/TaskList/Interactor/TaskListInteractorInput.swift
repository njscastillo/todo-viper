//
//  TaskListTaskListInteractorInput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import Foundation

protocol TaskListInteractorInput {
    func getTaskList(completion: ([Task]) -> Void)
    func isSolvedOn(taskId: String, completion: (Bool) -> Void)
    func isSolvedOff(taskId: String, completion: (Bool) -> Void)
    func addNewTask(completion: (Task) -> Void)
    func deleteTask(task: Task, completion: (Bool) -> Void)
}
