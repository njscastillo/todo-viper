//
//  TaskListTaskListPresenter.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class TaskListPresenter: TaskListModuleInput, TaskListInteractorOutput {

    // MARK: - Public Properties
    
    weak var view: TaskListViewInput!
    var interactor: TaskListInteractorInput!
    var router: TaskListRouterInput!
    
    // MARK: - Private Properties

    private var tasksList: [Task] = []
}

// MARK: - TaskListViewOutput

extension TaskListPresenter: TaskListViewOutput {
    func addNewTask() {
        interactor.addNewTask { (task) in
            tasksList.append(task)
            router.openDetail(task: task)
        }
    }
    
    func viewIsReady() {
        interactor.getTaskList { [weak self] (tasks) in
            self?.tasksList = tasks
            self?.view.set(items: tasksList)
            self?.view.reloadView()
        }
    }
}

// MARK: - TableViewAdapterOutput

extension TaskListPresenter: TableViewAdapterOutput {
    func delete(item: Task) {
        interactor.deleteTask(task: item) { (answer) in
            guard answer else { return }
            tasksList = tasksList.filter{ $0.id != item.id }
            view.set(items: tasksList)
        }
    }
    
    func select(item: Task) {
        router.openDetail(task: item)
    }
    
    func reloadData() {
    }
}

// MARK: - TaskListCellOutput

extension TaskListPresenter: TaskListCellOutput {
    func isSolvedOn(taskId: String, completion: (Bool) -> Void) {
        interactor.isSolvedOn(taskId: taskId) { (answer) in
            guard answer else { return }
            tasksList.first{ $0.id == taskId }?.isSolved = true
            view.set(items: tasksList)
            view.reloadView()
            completion(answer)
        }
    }
    
    func isSolvedOff(taskId: String, completion: (Bool) -> Void) {
        interactor.isSolvedOff(taskId: taskId) { (answer) in
            guard answer else { return }
            tasksList.first{ $0.id == taskId }?.isSolved = false
            view.set(items: tasksList)
            view.reloadView()
            completion(answer)
        }
    }
}
