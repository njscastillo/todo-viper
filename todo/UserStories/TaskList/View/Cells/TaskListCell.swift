//
//  TaskListCell.swift
//  todo
//
//  Created by Алексей Сулейманов on 23.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//

import UIKit

protocol TaskListCellInput {
    func setup(task: Task)
    func setup(output: TaskListCellOutput?)
}

protocol TaskListCellOutput {
    func isSolvedOn(taskId: String, completion: (Bool) -> Void)
    func isSolvedOff(taskId: String, completion: (Bool) -> Void)
}

class TaskListCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var taskDataTimeLabel: UILabel!
    @IBOutlet weak var taskImage: UIImageView!
    
    // MARK: Constants
    
    private enum Constants {
        static let uncheckImageName: String = "circle"
        static let checkImageName: String = "checkmark.circle.fill"
        static let dateFormatString: String = "dd MMM yyyy hh:mm"
    }
    
    // MARK: - Private Properties
    
    private (set) var task: Task? {
        didSet{
            taskNameLabel.text = self.task?.name
        }
    }
    private var isSolved: Bool = false {
        willSet {
            if newValue {
                checkButton.setImage(UIImage(systemName: Constants.checkImageName), for: .normal)
            } else {
                checkButton.setImage(UIImage(systemName: Constants.uncheckImageName), for: .normal)
            }
        }
    }
    private var output: TaskListCellOutput?
    
    // MARK: - Live Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - IBActions
    
    @IBAction func touchCheckButton(_ sender: Any) {
        if isSolved {
            output?.isSolvedOff(taskId: task?.id ?? "") { (answer) in
                guard answer else { return }
            }
        } else {
            output?.isSolvedOn(taskId: task?.id ?? ""){ (answer) in
                guard answer else { return }
            }
        }
    }
}

// MARK: - TaskListCellInput

extension TaskListCell: TaskListCellInput {
    
    func setup(task: Task) {
        self.task = task
        self.isSolved = task.isSolved
        self.taskImage.image = task.image
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.dateFormatString
        self.taskDataTimeLabel.text = dateFormatter.string(from: task.date ?? Date())
    }
    
    func setup(output: TaskListCellOutput?) {
        self.output = output
    }
}
