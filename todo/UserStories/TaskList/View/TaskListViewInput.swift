//
//  TaskListTaskListViewInput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

protocol TaskListViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState()
    func reloadView()
    func set(items: [Task])
    func set(image: UIImage, for indexPath: IndexPath)
}
