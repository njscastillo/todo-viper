//
//  PageNavigatorPageNavigatorPresenter.swift
//  todo
//
//  Created by // on 30/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class PageNavigatorPresenter: PageNavigatorInteractorOutput {

    // MARK: Public Properties
    
    weak var view: PageNavigatorViewInput!
    var interactor: PageNavigatorInteractorInput!
    var router: PageNavigatorRouterInput!

}

// MARK: - PageNavigatorViewOutput

extension PageNavigatorPresenter: PageNavigatorViewOutput {

    func viewIsReady() {
    }

}

// MARK: - PageNavigatorModuleInput

extension PageNavigatorPresenter: PageNavigatorModuleInput {

}
