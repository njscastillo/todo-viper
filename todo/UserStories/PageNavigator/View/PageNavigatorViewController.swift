//
//  PageNavigatorPageNavigatorViewController.swift
//  todo
//
//  Created by // on 30/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class PageNavigatorViewController: UIViewController, ModuleTransitionable {

    // MARK: - Public Properties

    var output: PageNavigatorViewOutput!

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
}

// MARK: PageNavigatorViewInput

extension PageNavigatorViewController: PageNavigatorViewInput {

        func setupInitialState() {
        }
        
    }
