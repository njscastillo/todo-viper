//
//  PageNavigatorPageNavigatorViewOutput.swift
//  todo
//
//  Created by // on 30/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol PageNavigatorViewOutput {

    /**
        @author //
        Notify presenter that view is ready
    */

    func viewIsReady()
}
