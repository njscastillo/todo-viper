//
//  PageNavigatorPageNavigatorConfigurator.swift
//  todo
//
//  Created by // on 30/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class PageNavigatorModuleConfigurator {

    func configure() -> PageNavigatorViewController {
       let bundle = Bundle(for: PageNavigatorViewController.self)
       guard let viewController = UIStoryboard(name: String(describing: PageNavigatorViewController.self),
                                     bundle: bundle).instantiateInitialViewController() as? PageNavigatorViewController else {
           fatalError("Can't load PageNavigatorViewController from storyboard, check that controller is initial view controller")
       }

        let router = PageNavigatorRouter()
        router.view = viewController

        let presenter = PageNavigatorPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = PageNavigatorInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController
    }

}
