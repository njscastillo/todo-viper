//
//  PageViewPageViewPresenter.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

class PageViewPresenter: PageViewInteractorOutput {

    // MARK: Public Properties
    
    weak var view: PageViewViewInput!
    var interactor: PageViewInteractorInput!
    var router: PageViewRouterInput!

    // MARK: Private Properties
    
    private var tasks: [Task] = []

}

// MARK: - PageViewViewOutput

extension PageViewPresenter: PageViewViewOutput {

    func viewIsReady() {
        interactor.getTaskList { (tasks) in
            var pages: [UIViewController] = []
            for task in tasks {
                let taskDatailController = TaskDetailModuleConfigurator().configure(with: task)
                pages.append(taskDatailController)
            }
            view.setupInitialState(pages: pages)
        }
    }
}

// MARK: - PageViewModuleInput

extension PageViewPresenter: PageViewModuleInput {

}
