//
//  TabsTabsPresenter.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

class TabsPresenter: TabsInteractorOutput {

    // MARK: Public Properties
    
    weak var view: TabsViewInput!
    var interactor: TabsInteractorInput!
    var router: TabsRouterInput!

    // MARK: Private Properties
    
    private var tasks: [Task] = []
}

// MARK: - TabsViewOutput

extension TabsPresenter: TabsViewOutput {
    func tabBarDidSelect(for tag: Int) {
        let taskDetailController = TaskDetailModuleConfigurator().configure(with: tasks[tag])
        taskDetailController.view.tag = tag
        view.set(viewController: taskDetailController)
    }
    

    func viewIsReady() {
        var items: [UITabBarItem] = []
        interactor.getTaskList { [weak self] (tasks) in
            self?.tasks = tasks
            tasks.enumerated().forEach { (tag,task) in
                let item = UITabBarItem(title: task.name, image: nil, tag: tag)
                items.append(item)
            }
            view.setupInitialState(items: items)
        }
    }
}

// MARK: - TabsModuleInput

extension TabsPresenter: TabsModuleInput {

}
