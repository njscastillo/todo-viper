//
//  TabsTabsViewInput.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

protocol TabsViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState(items: [UITabBarItem])
    func set(viewController: UIViewController)
    func showView(for tag: Int)
}
