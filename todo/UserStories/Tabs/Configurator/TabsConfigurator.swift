//
//  TabsTabsConfigurator.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TabsModuleConfigurator {

    func configure() -> TabsViewController {
       let bundle = Bundle(for: TabsViewController.self)
       guard let viewController = UIStoryboard(name: String(describing: TabsViewController.self),
                                     bundle: bundle).instantiateInitialViewController() as? TabsViewController else {
           fatalError("Can't load TabsViewController from storyboard, check that controller is initial view controller")
       }

        let router = TabsRouter()
        router.view = viewController
        
        let presenter = TabsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TabsInteractor()
        interactor.output = presenter
        interactor.taskListService = TaskListServiceRealm.shared

        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController
    }

}
