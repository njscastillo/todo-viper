//
//  testModuletestModuleConfigurator.swift
//  todo
//
//  Created by // on 03/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class testModuleModuleConfigurator {

    func configure() -> testModuleViewController {
       let bundle = Bundle(for: testModuleViewController.self)
       guard let viewController = UIStoryboard(name: String(describing: testModuleViewController.self),
                                     bundle: bundle).instantiateInitialViewController() as? testModuleViewController else {
           fatalError("Can't load testModuleViewController from storyboard, check that controller is initial view controller")
       }

        let router = testModuleRouter()
        router.view = viewController

        let presenter = testModulePresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = testModuleInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController
    }

}
