//
//  TaskDetailTaskDetailConfigurator.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TaskDetailModuleConfigurator {

    func configure(with task: Task) -> TaskDetailViewController {
    let bundle = Bundle(for: TaskDetailViewController.self)
    guard let viewController = UIStoryboard(name: String(describing: TaskDetailViewController.self),
                                  bundle: bundle).instantiateInitialViewController() as? TaskDetailViewController else {
        fatalError("Can't load ContactListViewController from storyboard, check that controller is initial view controller")
    }
    
        let router = TaskDetailRouter()
        router.view = viewController
        
        let presenter = TaskDetailPresenter(task: task)
        presenter.view = viewController
        presenter.router = router
        
        let interactor = TaskDetailInteractor()
        interactor.output = presenter
        interactor.taskListService = TaskListServiceRealm.shared

        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController
    }
}
