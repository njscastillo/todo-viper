//
//  TaskDetailTaskDetailInteractor.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

class TaskDetailInteractor: TaskDetailInteractorInput {


    // MARK: - Public Properties
    
    weak var output: TaskDetailInteractorOutput!
    var taskListService: TaskListServiceProtocol?
    
    // MARK: - TaskDetailInteractorInput
    
    func setTaskName(task: Task, newValue: String, completion: (Bool) -> Void) {
        taskListService?.setTaskName(taskId: task.id, newValue: newValue, completion: { (answer) in
            completion(answer)
        })
    }
    
    func setTaskDescription(task: Task, newValue: String, completion: (Bool) -> Void) {
        taskListService?.setTaskDescription(taskId: task.id, newValue: newValue, completion: { (answer) in
            completion(answer)
        })
    }
    
    func setTaskImage(task: Task, newValue: UIImage, completion: (Bool) -> Void) {
        taskListService?.setTaskImage(taskId: task.id, newValue: newValue, completion: { (answer) in
            completion(answer)
        })
    }
    
    func removeTaskImage(task: Task, completion: (Bool) -> Void) {
        taskListService?.removeTaskImage(taskId: task.id, completion: { (answer) in
            completion(answer)
        })
    }
    
}
