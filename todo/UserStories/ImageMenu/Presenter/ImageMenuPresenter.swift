//
//  ImageMenuImageMenuPresenter.swift
//  todo
//
//  Created by // on 25/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class ImageMenuPresenter: ImageMenuModuleInput, ImageMenuInteractorOutput {
    
    // MARK: - Public Properties
    
    weak var view: ImageMenuViewInput!
    var interactor: ImageMenuInteractorInput!
    var router: ImageMenuRouterInput!
    var output: ImageMenuModuleOutput!
    
}

// MARK: - ImageMenuViewOutput

extension ImageMenuPresenter: ImageMenuViewOutput {
    func showPage() {
        output.showPage()
    }
    
    func viewIsReady() {
        
    }
    
    func changeImage() {
        output.changeImage()
    }
    
    func removeImage() {
        output.removeImage()
    }
    
}
