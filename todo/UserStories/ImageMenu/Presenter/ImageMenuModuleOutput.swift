//
//  ImageMenuModuleOutput.swift
//  todo
//
//  Created by Алексей Сулейманов on 26.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//
import UIKit
import Foundation

protocol ImageMenuModuleOutput {
    func changeImage()
    func removeImage()
    func showPage()
}
