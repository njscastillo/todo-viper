//
//  ImageMenuImageMenuConfigurator.swift
//  todo
//
//  Created by // on 25/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class ImageMenuModuleConfigurator {
    func configure(with task: Task, output: ImageMenuModuleOutput? = nil) ->  ImageMenuViewController {
        let viewController = ImageMenuViewController()
        
        let router = ImageMenuRouter()
        router.view = viewController 
        
        let presenter = ImageMenuPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.output = output
        
        let interactor = ImageMenuInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController
    }
}
