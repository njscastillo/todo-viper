//
//  LoginLoginRouter.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

class LoginRouter: LoginRouterInput {
    
    weak var view: ModuleTransitionable?
    
    func showTodoList() {
        let storyboard = UIStoryboard(name: String(describing: TaskListViewController.self), bundle: nil)
        guard let taskListViewController = storyboard.instantiateInitialViewController() as? TaskListViewController else {
            fatalError("Can't load ChatListViewController from storyboard, check that controller is initial view controller")
        }
        view?.push(module: taskListViewController, animated: false)
    }
}
