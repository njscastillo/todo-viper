//
//  LoginLoginPresenter.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class LoginPresenter: LoginModuleInput, LoginInteractorOutput {
    
    // MARK: - Public Properties
    
    weak var view: LoginViewInput!
    var interactor: LoginInteractorInput!
    var router: LoginRouterInput!
    
    func viewIsReady() {

    }
}

// MARK: - LoginViewOutput

extension LoginPresenter: LoginViewOutput {
    func touchLogin() {
        view.getCredentials { (username, password) in
            let credentials = Credentials(username: username,password: password)
            interactor.authentication(credintals: credentials) { (answer) in
                guard answer == true else { return }
                router.showTodoList()
            }
        }
    }
}
