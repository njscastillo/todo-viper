//
//  TaskObject.swift
//  todo
//
//  Created by Алексей Сулейманов on 26.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//

import Foundation
import RealmSwift

class TaskObject: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var isSolved: Bool = false
    @objc dynamic var id : String = UUID().uuidString
    @objc dynamic var date : Date = Date()
    @objc dynamic var image: Data?
    @objc dynamic var descrip: String?
}
