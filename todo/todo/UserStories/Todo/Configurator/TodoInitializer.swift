//
//  TodoTodoInitializer.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TodoModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var todoViewController: TodoViewController!

    override func awakeFromNib() {

        let configurator = TodoModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: todoViewController)
    }

}
