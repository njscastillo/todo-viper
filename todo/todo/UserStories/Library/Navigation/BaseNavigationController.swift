//

import UIKit

final class BaseNavigationController: UINavigationController {

    // MARK: - UIKit

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        configureAppearance()
    }

    // MARK: - Private Methods

    func configureAppearance() {
//        let backImage = Asset.leftArrow.image.withRenderingMode(.alwaysTemplate)
//        navigationBar.backIndicatorImage = backImage
//        navigationBar.backIndicatorTransitionMaskImage = backImage
    }
}

// MARK: - UINavigationControllerDelegate

extension BaseNavigationController: UINavigationControllerDelegate {

    func navigationController(_ navigationController: UINavigationController,
                              willShow viewController: UIViewController,
                              animated: Bool) {
        UIView.performWithoutAnimation {
            let barButton = UIBarButtonItem(title: "",
                                            style: .plain,
                                            target: nil,
                                            action: nil)
            navigationController.topViewController?.navigationItem.backBarButtonItem = barButton
        }
    }
}
