//
//  LoginLoginPresenter.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class LoginPresenter: LoginModuleInput, LoginInteractorOutput {

    weak var view: LoginViewInput!
    var interactor: LoginInteractorInput!
    var router: LoginRouterInput!

    func viewIsReady() {

    }
}

// MARK: - LoginViewOutput

extension LoginPresenter: LoginViewOutput {
    func touchLoginButton() {
        view.getCredentials { (login, password) in
            guard
                let login = login,
                let password = password
                else { return }
            
            let credintals = Credintals(login: login, password: password)
            interactor.authentication(credintals: credintals) { (answer) in
                guard !answer == true else { return }
                router.showTodoList()
            }
        }
    }
}
