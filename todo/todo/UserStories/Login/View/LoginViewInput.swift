//
//  LoginLoginViewInput.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

protocol LoginViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState()
    func getCredentials(completion: (String?, String?) -> Void)
    
}
