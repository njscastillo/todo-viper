//
//  LoginService.swift
//  todo
//
//  Created by Алексей Сулейманов on 25.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//

import Foundation

protocol LoginServiceProtocol {
    func authentication(credintals: Credentials, completion: (Bool) -> Void)
}

class LoginServiceImp: LoginServiceProtocol {
    
    
    // MARK: - Singleton
    
    static var shared = LoginServiceImp()
    private init() {}
    
    // MARK: Public Properties
    
    var users: [User] = [User(login: "alex", password: "alex"),
                         User(login: "atasya", password: "atasya")]

    // MARK: - LoginServiceProtocol

    func authentication(credintals: Credentials, completion: (Bool) -> Void) {
        guard let user = (users.first { $0.login == credintals.username })
            else {
                completion(false)
                return
        }
        guard user.password == credintals.password
            else {
                completion(false)
                return
        }
        completion(true)
    }
}
