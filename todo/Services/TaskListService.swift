//
//  TaskListService.swift
//  todo
//
//  Created by Алексей Сулейманов on 23.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//
import UIKit
import Foundation

protocol TaskListServiceProtocol {
    func getTaskList(completion: ([Task]) -> Void)
    func isSolvedOn(taskId: String, completion: (Bool) -> Void)
    func isSolvedOff(taskId: String, completion: (Bool) -> Void)
    func setTaskName(taskId: String, newValue: String, completion: (Bool) -> Void)
    func setTaskDescription(taskId: String, newValue: String, completion: (Bool) -> Void)
    func addNewTask(completion: (Task) -> Void)
    func deleteTask(task: Task, completion: (Bool) -> Void)
    func setTaskImage(taskId: String, newValue: UIImage, completion: (Bool) -> Void)
    func removeTaskImage(taskId: String, completion: (Bool) -> Void)
}

class TaskListServiceImp: TaskListServiceProtocol {
    
    // MARK: - Constants
    
    enum Constants {
        static var newTaskDefaultName = "New Task"
    }
    
    static var shared = TaskListServiceImp()
    
    private init() {}
    
    var tasks: [Task] = [Task(name: "Foo", isSolved: false),
                         Task(name: "Bar", isSolved: true)]
    
    func getTaskList(completion: ([Task]) -> Void) {
        completion(tasks)
    }
    
    func isSolvedOn(taskId: String, completion: (Bool) -> Void) {
        tasks.first{ $0.id == taskId }?.isSolved = true
        completion(true)
    }
    
    func isSolvedOff(taskId: String, completion: (Bool) -> Void) {
        tasks.first{ $0.id == taskId }?.isSolved = false
        completion(true)
    }

    func setTaskName(taskId: String, newValue: String, completion: (Bool) -> Void) {
        tasks.first{ $0.id == taskId }?.name = newValue
        completion(true)
    }
    
    func setTaskDescription(taskId: String, newValue: String, completion: (Bool) -> Void) {
        tasks.first{ $0.id == taskId }?.description = newValue
        completion(true)
    }
    
    func addNewTask(completion: (Task) -> Void) {
        let task = Task(name: Constants.newTaskDefaultName)
        tasks.append(task)
        completion(task)
    }
    
    func deleteTask(task: Task, completion: (Bool) -> Void) {
        tasks = tasks.filter { $0.id != task.id }
        completion(true)
    }
    
    func setTaskImage(taskId: String, newValue: UIImage, completion: (Bool) -> Void) {
        tasks.first{ $0.id == taskId }?.image = newValue
        completion(true)
    }
    
    func removeTaskImage(taskId: String, completion: (Bool) -> Void) {
        tasks.first{ $0.id == taskId }?.image = nil
        completion(true)
    }
}
